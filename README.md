### 

```shell
python3 -m venv .venv
. .venv/bin/activate
python3 -m pip install ansible
```

```shell
ansible-playbook ./playbooks/debug.yaml -i .inventory/test.yaml
```

Удалить если уже были

```shell
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.40"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.41"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.42"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.43"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.44"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.45"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.46"
ssh-keygen -f "/home/ivan/.ssh/known_hosts" -R "192.168.0.47"
```

Копируем kube-конфиг на локаль

```shell
scp root@192.168.0.40:~/.kube/config ~/.kube/clusters/k1
```

Поменять название контекста и название пользователя (по умолчанию будет пересекаться с дургими)


```toml
    [plugins."io.containerd.grpc.v1.cri".registry]
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
          endpoint = ["https://docker-mirror.nexus.ers.net.ru"]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."gcr.io"]
          endpoint = ["https://docker-mirror.nexus.ers.net.ru"]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."k8s.gcr.io"]
          endpoint = ["https://docker-mirror.nexus.ers.net.ru"]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry.k8s.io"]
          endpoint = ["https://docker-mirror.nexus.ers.net.ru"]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."ghcr.io"]
          endpoint = ["https://docker-mirror.nexus.ers.net.ru"]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."quay.io"]
          endpoint = ["https://docker-mirror.nexus.ers.net.ru"]
```